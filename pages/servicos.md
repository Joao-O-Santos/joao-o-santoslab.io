# Serviços

[Click here for the English version.](./services.html)



## Consultadoria

**Se gostaria de ajuda a planear estudos e/ou aprender a partir dos
dados, eu posso ajudá-lo/a nesse processo.** Tenho mais de cinco anos de
experiência a planear estudos, analisar dados e a retirar conclusões
suportadas pelos mesmos. Caso queira comunicar os seus resultados mais
claramente, **eu posso ajudar a gerar gráficos, relatórios e outros
materiais para o ajudar a captar a sua audiência. Também podemos
trabalhar juntos sobre a melhor forma de apresentar os dados e sobre
como melhorar a sua apresentação para um público (*public speaking*).**
O Mundo de hoje está cheio de ferramentas computacionais com um
potencial incrível, que frequentemente não são exploradas na academia
nem na indústria. **Eu posso ajudá-lo a automatizar tarefas repetitivas
e encontrar formas de desenvolver *software* ajustado às suas
necessidades ou às do seu negócio (e.g., desenvolvendo aplicações
`R/Shiny` personalizadas).**



## Tutoria/Explicações

**Se é um/a investigador júnior ou sénior à procura de dominar o `R`
e/ou modelos mistos/multinível, eu posso oferecer tutoria
personalizada.**



## Ensino

**Adoro ensinar filosofia da ciência, métodos de investigação,
estatística e psicologia social.** Já dei aulas a todos os ciclos
universitários, licenciatura, mestrado e doutoramento, dar aulas
a qualquer um desses ciclos foi uma experiência muito recompensadora.
A filosofia da ciência, os métodos de investigação e a estatística são
muitas vezes ensinados em unidades curriculares estanques, mesmo estando
profundamente relacionados, tanto a um nível conceptual como prático.
A longo prazo, isso é algo que eu gostava muito que mudasse
e trabalharei apaixonadamente para fazer a minha parte para alterar.
No entretanto, **eu vou esforçando-me por salientar a relação entre
filosofia da ciência, métodos de investigação, e estatística, sempre que
lecciono algum dos temas e mesmo quando apresento resultados de estudos
da psicologia social.** Se este é um tipo de abordagem que a sua
instituição valorizaria, por favor contacte-me. **Há mais de quatro anos
que tenho ensinado `R` a psicólogo/as e pessoas que trabalham em áreas
relacinados. Desde aí que tenho organizado e estou activamente
a construir materiais acessíveis e apelativos para essas audiências.**
Se gostaria de organizar um workshop sobre `R`, estatística, modelos
mistos/multinível, ou mesmo uma escola de verão, na sua instituição, por
favor, contacte-me.


## Contactos e Orçamento

Se gostaria que eu preparasse uma proposta de orçamento, por favor,
[envie-me um email](mailto:joao.filipe.santos@campus.ul.pt), para que
possamos agendar uma reunião e discutir os detalhes.

Também estou [disponível no
LinkedIn](https://www.linkedin.com/services/page/1400b3320144111956)
e no [zaask](https://www.zaask.pt/user/joaoosantos).

<a target="_blank" href="https://www.zaask.pt/user/joaoosantos">
<img src="https://www.zaask.pt/widget?user=740474&widget=pro-findme" alt="selo zaask">
</a>
