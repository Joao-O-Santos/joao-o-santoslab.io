<!-- 0 MENU_ENTRY=Home -->
# Welcome

<img class="center_img" src="./images/jos.png" alt="a photo of me">

Hi!

**I'm João (John), a social psychology PhD student, as well as
a statistics/`R` programming consultant and tutor, with a great interest
in connecting philosophy of science, research methods, and data
analysis.** My research has focused on how adults perceive children, as
a social category. My passion for programming and IT has led me to my
freelance work, where I leverage technology to solve problems and
automate tasks, in academia and in the industry.

If you want to learn more about me, checkout the [about
me](about_me.html)
page, take a look at my [CV](https://joao-o-santos.gitlab.io/cv), my
[portfolio](portfolio.html), and/or my
[GitLab profile](https://gitlab.com/joao-o-santos).
