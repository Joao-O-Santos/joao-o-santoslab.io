<!-- 1 MENU_ENTRY=About Me -->
# About Me

<img class="center_img" src="./images/jos.png" alt="a photo of me">

I'm a social psychology PhD student and statistics consultant/tutor. I'm
fascinated by how we can learn about the natural world, as well as
figure out how sure and how humble we should be about what we find.

My research has focused on how adults perceive children, as a social
category. Exploring ageism across the age spectrum, or other less
debated forms of discrimination (e.g., ableism, the treatment
of prisoners, etc...), is something I would like to do in the future
should the opportunity arise.

I'm increasingly drawn into figuring out ways to leverage technology to
solve problems, in academia and in the industry. I've used and developed
software to analyze data efficiently, teach statistics, and automate
trivial tasks. Teaching and tutoring people about `R`, statistics,
research methods is also something I love to do, be it in a classroom or
in individual lessons.

I love `Linux`, programming (specially in `C`), learning about computer
science, particularly about hardware-software interfaces and systems
programming. Besides programming, I enjoy playing around with several
random hobbies that bring me joy and get me away from the computer. I've
been blessed with an amazing son and an amazing wife, that's one of the
reasons why I prefer working as a freelancer (remotely or hybrid), so
I can spend more time with them.
