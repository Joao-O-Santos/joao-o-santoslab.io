# Services

[Clique aqui para a versão em Português.](./servicos.html)



## Consulting

**If you would like help planning studies and/or learning from data,
I can help you along the way.** I have over five years of experience
designing studies, analysing data, and drawing reasonable conclusions
from it. In case you want to communicate your results more clearly,
**I can help generate graphs, reports, and other materials to help you
engage your audience. We can also work together on how to present your
data, and improve your public speaking.** Today's world is full of
computer tools with incredible potential, that often go unexplored in
academia nor in the industry. **I can help you automate repetitive
tasks, and find ways to develop tailored software to your personal or
business needs (e.g., developing custom `R/Shiny` apps).**



## Tutoring

**If you're a junior or senior researcher wanting to master `R` and/or
mixed/multilevel models, I can provide personalized tutoring.**


## Teaching

**I love teaching about philosophy of science, research methods,
statistics, and social psychology.** I've taught at every university
level, bachelors, masters, and PhD, and teaching any of those levels
brought me joy. Philosophy of science, research methods, and statistics
are mostly taught separately, despite being intertwined at a conceptual
and practical level. In the long run, this is something I want to
see changed, and I'm passionate to do my part to change it. In the
meanwhile, **I strive to highlight the relation between philosophy of
science, research methods, and statistics, whenever I teach any of those
topics, and even when I present findings from social psychology
studies.** If this kind of approach is something your or your
institution would value, please reach out. **I've also been teaching `R`
to psychologists, and people working on related fields, for over four
years now. I've been collecting and actively preparing approachable and
compelling materials to reach those audiences ever since.** If you would
like to host a workshop on `R`, statistics, mixed/multilevel models, or
even a summer school, at your institution, please reach out.



## Contact and Pricing

If you like me do draft a budget proposal, please [reach out over
email](mailto:joao.filipe.santos@campus.ul.pt), so we can set up
a meeting and discuss details.

I'm also [available on
LinkedIn](https://www.linkedin.com/services/page/1400b3320144111956) and
on [zaask](https://www.zaask.pt/user/joaoosantos).
