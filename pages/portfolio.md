<!-- 3 MENU_ENTRY=Portfolio -->
# Portfolio

Below is a list of projects I've worked on but might not be described or
featured in my [cv](https://joao-o-santos.gitlab.io/cv).

*Note: This page is a work in progress, there may be other projects
I would like to share with you, but that I'm still polishing a bit more
before making them public.*

**To see what I'm currently working on you can take a look at my [GitLab
profile](https://gitlab.com/joao-o-santos). To see a more formal resume
you can take a look at my [cv](https://joao-o-santos.gitlab.io/cv).**



## Academic


### Consulting/Tutoring

I've consulted on a research project, and tutored a master's student, as
well as a post-doc, on how to use `R` for analysing experimental data,
with a focus on leveraging linear mixed models.

<!--

### `iatgen`-generated IATs translator

Together with Cristina Mendonça, Sara Hagá, and Emerson Do Bú, I've
reached out to `iatgen`'s original authors in order to implement
a translation feature, so that `iatgen` can generate IATs in
user-selected languages.

-->

### QHELP

I've collaborated with Sérgio Moreira, worked with and mentored
students in developing `R`/`Shiny` apps for teaching about linear models
and their assumptions, in undergraduate and graduate statistics classes.


### RUGGED

I helped establish and coordinate an `R` user group named
[RUGGED](https://rggd.gitlab.io). The group is made up of people with
varying skill levels, mostly studying or working in psychology and
neighboring fields, that want to learn and practice `R` together. If
this sounds like a group you would be interested in joining just
[see the group's onboarding
page](https://rggd.gitlab.io/onboarding.html).

I'm also spearheading the development of the [group's
projects](https://rggd.gitlab.io/projects.html). To save you a trip to
the [RUGGED project's page](https://rggd.gitlab.io/projects.html),
I summarize them below.

#### MOC

**MOC** is our ongoing attempt at building a *Medium Online Course* on
using `R` for data analysis in psychology and related fields. Currently,
we are working on drafting an online `RMarkdown` book, with exercises
and accompanying materials. The [unfinished draft is available
online](https://rggd.gitlab.io/moc) and is automatically updated and
deployed as a GitLab Pages' page.

#### SciOps

**SciOps** is our ongoing attempt to create a template (or templates)
to help researchers leverage cutting-edge tools to increase their
productivity, by automating repetitive tasks, as well as their
research's reproducibility and transparency.

#### RUGGED's website

The [source code for RUGGED's
website](https://gitlab.com/rggd/rggd.gitlab.io) is also public, built
and deployed with [`make
it_stop`](https://gitlab.com/joao-o-santos/make-it-stop).



## Industry


### SPGrow360

I've collaborated with Sérgio Moreira and Cristina Mendonça to automate
the process of using Sérgio's 360 worker social performance evaluation
tool---SPGrow360. Sérgio has ongoing collaborations with industry
partners to deploy his tool commercially for interested companies.



## Hobby Projects

You can find my hobby projects on my
[GitLab](https://gitlab.com/joao-o-santos), below are some highlights
and descriptions.


### make it\_stop

I've been trying to streamline the use of `GNU Make` as a static website
generator to render static websites, that can be served as `GitLab
Pages`. I'm calling it the `make it_stop` static website generator. In
fact this website is made it. You can follow my efforts
[here](https://gitlab.com/joao-o-santos/make-it-stop).


### dotfiles

I've also started hosting [my
dotfiles](https://gitlab.com/joao-o-santos/dotfiles) with the
configuration for most of the tooling that I use. These include a custom
template to allow `pandoc` to convert `markdown` files to mostly APA 7th
compliant `.docx` files.


### Personal Website

The [source code for this website](https://gitlab.com/Joao-O-Santos/joao-o-santos.gitlab.io)
is public, built and deployed with [`make
it_stop`](https://gitlab.com/joao-o-santos/make-it-stop).


### CV

The [source code for my CV ](https://gitlab.com/Joao-O-Santos/cv) is
also public, and automatically built and deployed as a GitLab Pages
page.
